// 开发环境接口配置
const APIURl = 'http://192.168.5.86:30000'
const WEB_URL='http://192.168.5.86/DataV'
module.exports = {
  apiURI: APIURl,
  comUploadUrl: WEB_URL + '/api/visualdev/DataScreen/Images/',
  comUrl: process.env.VUE_APP_BASE_API,
  staticPath: process.env.NODE_ENV === 'development' ? '' : '/dataV'
}